/*!
 * Fabric <https://github.com/webcthulhu/fabric-editor>
 */
'use strict';

const { fabric: Fabric } = require('fabric');

const SRC_SHIFT = 25;
const SRC_ZOOM_VALUES = [
    .1, .125, .15, .175, .2, .225, .25, .275, .3, .325, .35, .375, .4, .425, .45, .475, .5,
    .525, .55, .575, .6, .625, .65, .675, .7, .725, .75, .775, .8, .825, .85, .875, .9, .925, .95, .975, 1
];
const OPTIONS = {
  CANVAS: {
    backgroundColor: '#ffffff',
    controlsAboveOverlay: true,
    imageSmoothingEnabled: false,
    preserveObjectStacking: true,
    selection: false,
    skipTargetFind: false,
    stateful: false
  },
  OBJECT: {
    centeredScaling: true,
    objectCaching: false,
    originX: 'center',
    originY: 'center',
    cornerColor: 'rgba(0,0,0,0)',
    cornerSize: 0,
    cornerStyle: 'circle',
    hasBorders: false,
    hasControls: false,
    hasRotatingPoint: false,
    lockMovementX: true,
    lockMovementY: true,
    lockRotation: true,
    lockScalingX: true,
    lockScalingY: true,
    lockScalingFlip: true,
    lockSkewingX: true,
    lockSkewingY: true,
  },
  IMAGE: {
    crossOrigin: 'anonymous'
  },
  PATHGROUP: {
    backgroundColor: '#ffffff',
    crossOrigin: 'anonymous',
    fill: '#000000',
    srcUrl: null,
    toBeParsed: false,
    toObject: function() {
      return Fabric.util.object.extend(this.callSuper('toObject'), {
        cellId: this.get('cellId'),
        srcUrl: this.get('srcUrl'),
        uniqueId: this.get('uniqueId')
      });
    },
    drawObject: function(ctx) {
      ctx.save();
      ctx.fillStyle = this.backgroundColor;
      ctx.fillRect(-this.width / 2, -this.height / 2, this.width, this.height);
      const minScale = Math.min(this.scaleX, this.scaleY);
      const x = this.scaleX / minScale;
      const y = this.scaleY / minScale;
      ctx.scale(1 / x, 1 / y);
      ctx.translate(-this.width / 2, -this.height / 2);
      for (let i = 0, l = this.paths.length; i < l; ++i) {
        this.paths[i].render(ctx, true);
      }
      ctx.restore();
    },
    updateSourceBox: function() { return this; }
  },
  RECT: {
    fill: '#EEEEEE',
    strokeWidth: 0
  }
};
Fabric.util.object.extend(Fabric.Canvas.prototype, OPTIONS.CANVAS);
Fabric.util.object.extend(Fabric.Object.prototype, OPTIONS.OBJECT);
Fabric.util.object.extend(Fabric.Image.prototype, OPTIONS.IMAGE);
Fabric.util.object.extend(Fabric.PathGroup.prototype, OPTIONS.PATHGROUP);
Fabric.util.object.extend(Fabric.Rect.prototype, OPTIONS.RECT);

Fabric.PathGroup.fromObject = function(object, callback) {
  Fabric.loadSVGFromURL(object.srcUrl, function(elements, options) {
    const instance = new Fabric.PathGroup(elements, options);
    instance.set({
      cellId: object.cellId,
      srcUrl: object.srcUrl,
      uniqueId: object.uniqueId,
      top: object.top,
      left: object.left,
      scaleX: object.scaleX,
      scaleY: object.scaleY,
      fill: object.fill,
      backgroundColor: object.backgroundColor
    });
    callback(instance);
  });
};

const CollageCell = Fabric.util.createClass(Fabric.Rect, {
  type: 'collageCell',
  empty: true,
  cellId: 0,
  initialize: function(options) {
    this.callSuper('initialize', options);
  },
  toObject: function() {
    return Fabric.util.object.extend(this.callSuper('toObject'), {
      cellId: this.get('cellId'),
      empty: this.get('empty')
    });
  },
  updateSourceBox: function() {
    return this;
  }
});

CollageCell.fromObject = function(object, callback, forceAsync) {
  return Fabric.Object._fromObject('CollageCell', object, callback, forceAsync);
};

const CollageImage = Fabric.util.createClass(Fabric.Image, {
  type: 'collageImage',
  sourceBox: { x: 0, y: 0, height: 0, width: 0 },
  sourceCenter: { x: 0, y: 0 },
  sourceZoom: 1,
  initialize: function(element, options) {
    this.callSuper('initialize', element, options);
    options = options || {};
    this.originalWidth = options.originalWidth;
    this.originalHeight = options.originalHeight;
    this.sourceCenter = options.sourceCenter || { x: this._element.naturalWidth / 2, y: this._element.naturalHeight / 2 };
    this.sourceZoom = options.sourceZoom || 1;
    this.updateSourceBox();
  },
  toObject: function() {
    return Fabric.util.object.extend(this.callSuper('toObject'), {
      cellId: this.get('cellId'),
      uniqueId: this.get('uniqueId'),
      sourceBox: this.get('sourceBox'),
      sourceCenter: this.get('sourceCenter'),
      sourceZoom: this.get('sourceZoom'),
      originalWidth: this.get('originalWidth'),
      originalHeight: this.get('originalHeight'),
      imageNaturalWidth: this._element.naturalWidth
    });
  },
  _render: function(ctx) {
    let elementToDraw;
    if (this.isMoving === false && this.resizeFilters && this.resizeFilters.length && this._needsResize()) {
      elementToDraw = this.applyFilters(null, this.resizeFilters, this._filteredEl || this._originalElement, true);
    } else {
      elementToDraw = this._element;
    }
    if (elementToDraw) {
      ctx.drawImage(
        elementToDraw,
        this.sourceBox.x, this.sourceBox.y, this.sourceBox.width, this.sourceBox.height,
        -this.width / 2, -this.height / 2, this.width, this.height
      );
    }
    this._renderStroke(ctx);
  },
  applyFilters: function(callback, filters, imgElement, forResizing) {
    filters = filters || this.filters;
    imgElement = imgElement || this._originalElement;
    if (!imgElement) {
      return;
    }
    const replacement = Fabric.util.createImage(),
      retinaScaling = this.canvas ? this.canvas.getRetinaScaling() : Fabric.devicePixelRatio,
      minimumScale = this.minimumScaleTrigger / retinaScaling,
      _this = this;
    let scaleX, scaleY;
    if (filters.length === 0) {
      this._element = imgElement;
      if (callback) { callback(this); }
      return imgElement;
    }
    let canvasEl = Fabric.util.createCanvasElement();
    canvasEl.width = imgElement.naturalWidth;
    canvasEl.height = imgElement.naturalHeight;
    canvasEl.getContext('2d').drawImage(imgElement, 0, 0, imgElement.naturalWidth, imgElement.naturalHeight);
    filters.forEach(function(filter) {
      if (!filter) {
        return;
      }
      if (forResizing) {
        scaleX = _this.scaleX < minimumScale ? _this.scaleX : 1;
        scaleY = _this.scaleY < minimumScale ? _this.scaleY : 1;
        if (scaleX * retinaScaling < 1) {
          scaleX *= retinaScaling;
        }
        if (scaleY * retinaScaling < 1) {
          scaleY *= retinaScaling;
        }
      } else {
        scaleX = filter.scaleX;
        scaleY = filter.scaleY;
      }
      filter.applyTo(canvasEl, scaleX, scaleY);
      if (!forResizing && filter.type === 'Resize') {
        _this.width *= filter.scaleX;
        _this.height *= filter.scaleY;
      }
    });
    replacement.width = canvasEl.width;
    replacement.height = canvasEl.height;
    if (Fabric.isLikelyNode) {
      replacement.src = canvasEl.toBuffer(undefined, Fabric.Image.pngCompression);
      _this._element = replacement;
      if (!forResizing) { _this._filteredEl = replacement };
      if (callback) { callback(_this); }
    } else {
      replacement.onload = function() {
        _this._element = replacement;
        if (!forResizing) { _this._filteredEl = replacement; }
        if (callback) { callback(_this); }
        replacement.onload = canvasEl = null;
      };
      replacement.src = canvasEl.toDataURL('image/png');
    }
    return canvasEl;
  },
  shiftSource: function (value) {
    if (!this) { return; }
    let index, x = 0, y = 0;
    switch (value) {
      case 'up': y += SRC_SHIFT * this.sourceZoom; break;
      case 'down': y -= SRC_SHIFT * this.sourceZoom; break;
      case 'left': x += SRC_SHIFT * this.sourceZoom; break;
      case 'right': x -= SRC_SHIFT * this.sourceZoom; break;
      case 'center': x = this._element.naturalWidth / 2; y = this._element.naturalHeight / 2; break;
      case 'in':
        index = SRC_ZOOM_VALUES.indexOf(this.sourceZoom);
        if (index > 0) { this.sourceZoom = SRC_ZOOM_VALUES[index - 1]; }
        break;
      case 'out':
        index = SRC_ZOOM_VALUES.indexOf(this.sourceZoom);
        if (index < SRC_ZOOM_VALUES.length - 1) { this.sourceZoom = SRC_ZOOM_VALUES[index + 1]; }
        break;
    }
    this.shiftSourceBy(x, y);
  },
  shiftSourceBy: function(x, y, z) {
    const maxScale = Math.max(this.scaleX, this.scaleY),
      multiX = this.scaleX / maxScale,
      multiY = this.scaleY / maxScale,
      width = this.sourceZoom * this._element.naturalWidth * multiX,
      height = this.sourceZoom * this._element.naturalHeight * multiY;
    this.sourceCenter.x += x;
    this.sourceCenter.y += y;
    this.updateSourceBox();
    this.sourceBox = {
      x: Math.floor(this.sourceCenter.x - width / 2),
      y: Math.floor(this.sourceCenter.y - height / 2),
      width: Math.floor(width),
      height: Math.floor(height)
    };
  },
  updateSourceBox: function() {
    const elementToDraw = this._element,
      scaleX = this.scaleX,
      scaleY = this.scaleY,
      maxScale = Math.max(scaleX, scaleY),
      multiX = scaleX / maxScale,
      multiY = scaleY / maxScale,
      naturalWidth = elementToDraw.naturalWidth,
      naturalHeight = elementToDraw.naturalHeight,
      width = this.sourceZoom * naturalWidth * multiX,
      height = this.sourceZoom * naturalHeight * multiY;
    this.sourceBox = {
      x: Math.floor(this.sourceCenter.x - width / 2),
      y: Math.floor(this.sourceCenter.y - height / 2),
      width: Math.floor(width),
      height: Math.floor(height)
    };
    if ((this.sourceCenter.y - height / 2) < 0) { this.sourceCenter.y = height / 2; }
    if ((this.sourceCenter.y + height / 2) > naturalHeight) { this.sourceCenter.y = naturalHeight - height / 2; }
    if ((this.sourceCenter.x - width / 2) < 0) { this.sourceCenter.x = width / 2; }
    if ((this.sourceCenter.x + width / 2) > naturalWidth) { this.sourceCenter.x = naturalWidth - width / 2; }
    return this;
  }
});
CollageImage.async = true;
CollageImage.fromObject = function (object, callback) {
  try {
    const img = document.querySelector('img[src="' + object.src + '"]');
    if (callback) { callback(new Fabric.CollageImage(img, object)); }
  } catch (err) {
    Fabric.util.loadImage(object.src, function (img) {
      if (callback) { callback(new Fabric.CollageImage(img, object)); }
    }, null, object.crossOrigin);
  }
};

Fabric.CollageCell = CollageCell;
Fabric.CollageImage = CollageImage;
Fabric.Object.NUM_FRACTION_DIGITS = 8;

module.exports = Fabric;